const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');
const mongo = require('./src/connections/mongodb.connection');

const express = require('express');
const app = express();
const port = process.env.PORT || 4004;

const authenticationRoutes = require('./src/routes/authentication.routes');
const userRoutes = require('./src/routes/user.routes');

const bodyParser = require('body-parser');

const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/authentication', authenticationRoutes);
app.use('/user', userRoutes);

// default:
app.use('*', (req, res) => {
  res.status(400).send({
    error: 'not available',
  });
});

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.logger.level('debug');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
