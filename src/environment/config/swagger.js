const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Authentication Management',
      version: '1.0.0',
      description: 'Swagger documentation for Authentication Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;
