const express = require('express');
const routes = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const secretKey = require('../environment/config/definitlynotsecret.config').secretKey;
const User = require('../models/user.model');

const createToken = user => {
  const payload = {
    user: user,
  };

  return jwt.sign(payload, secretKey, {
    expiresIn: '1h',
  });
};

/**
 * @swagger
 * /api/authentication-management/authentication/:
 *    post:
 *     tags:
 *       - authentication
 *     description: Returns authentication token if user is valid.
 *     parameters:
 *        - in: body
 *          name: body
 *          description: Find existing user object.
 *          schema:
 *            type: object
 *            required: true
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Authentication token is returned.
 *       401:
 *          description: Invalid user details.
 */
routes.post('/', (req, res) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (user != null) {
        bcrypt
          .compare(req.body.password, user.password)
          .then(valid => {
            if (!valid) {
              res.status(401).send({ error: 'Invalid credentials' });
            }

            res.json({
              token: createToken(user),
            });
          })
          .catch(error => {
            res.status(500).send(error);
          });
      } else {
        res.status(401).send({ error: 'Invalid credentials' });
      }
    })
    .catch(error => {
      res.status(500).send(error);
    });
});

module.exports = routes;
