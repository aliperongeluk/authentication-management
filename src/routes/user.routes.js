const express = require('express');
const routes = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../models/user.model');
const rp = require('request-promise');

/**
 * @swagger
 * /api/authentication-management/user/:
 *    post:
 *     tags:
 *       - authentication
 *     description: Registers user in mongodb database.
 *     parameters:
 *        - in: body
 *          name: body
 *          description: Create new user object.
 *          schema:
 *            type: object
 *            required: true
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *              roleId:
 *                type: number
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: User registered.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/', (req, res) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (user == null) {
        bcrypt
          .hash(req.body.password, bcrypt.genSaltSync(10))
          .then(hashedPassword => {
            const newUser = new User({
              email: req.body.email,
              password: hashedPassword,
              roleId: req.body.roleId,
            });

            newUser
              .save()
              .then(createdUser => {
                const options = {
                  method: 'POST',
                  uri: 'http://zuul-apigateway:5005/api/customer-information-management/customers/',
                  // uri: 'http://localhost:7003/customers/',
                  body: {
                    email: req.body.email,
                    authId: createdUser._id,
                  },
                  json: true, // Automatically stringifies the body to JSON
                };

                rp(options)
                  .then(() => res.status(200).json(createdUser))
                  .catch(error => {
                    console.error('request failed ' + error);

                    User.findByIdAndRemove(createdUser._id)
                      .then(() => console.log('removed newUser from db because of customer-management failure'))
                      .catch(() => console.error('removing newUser failed'));

                    res.status(500).json(error);
                  });
              })
              .catch(error => {
                console.error('save user failed ' + error);
                res.status(500).json(error);
              });
          })
          .catch(error => {
            console.error('bcrypt hash failed ' + error);
            res.status(500).json(error);
          });
      } else {
        res.status(400).json({
          message: 'Something went wrong. Possibly, a user with the given email already exists.',
        });
      }
    })
    .catch(error => {
      console.error('finding user failed');
      res.status(500).json(error);
    });
});

module.exports = routes;
